import { ArgumentsHost } from '@nestjs/common';
import { BaseWsExceptionFilter } from '@nestjs/websockets';
export declare class SocketExceptionFilter extends BaseWsExceptionFilter {
    private readonly logger;
    constructor();
    catch(exception: any, host: ArgumentsHost): void;
    errorMessage(exception: any, host: ArgumentsHost): any;
    WsErrorToThrow(code: string): {
        multiLanguageError: any;
        code: string;
    };
}
