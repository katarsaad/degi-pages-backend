import { ArgumentsHost, ExceptionFilter, HttpException } from '@nestjs/common';
export declare class HttpExceptionFilter implements ExceptionFilter {
    private readonly logger;
    constructor();
    catch(exception: HttpException, host: ArgumentsHost): any;
    HttpErrorToThrow(code: string): {
        multiLanguageError: any;
        code: string;
    };
}
