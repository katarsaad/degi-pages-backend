"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PrismaClientExceptionFilter = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const client_1 = require("@prisma/client");
let PrismaClientExceptionFilter = class PrismaClientExceptionFilter extends core_1.BaseExceptionFilter {
    constructor() {
        super(...arguments);
        this.logger = new common_1.Logger('PrismaClientExceptionFilter');
    }
    catch(exception) {
        const message = exception.message.replace(/\n/g, '');
        const value = JSON.stringify(exception.meta.value);
        const target = JSON.stringify(exception.meta.target);
        this.logger.error(message);
        switch (exception.code) {
            case 'P1010': {
                throw new common_1.ForbiddenException("User don't have access on database");
            }
            case 'P2000': {
                throw new common_1.BadRequestException(`The provided value for the column is too long for the column's type. Column: ${target} Given value: ${value},`);
            }
            case 'P2002': {
                throw new common_1.ConflictException(`The field ${target} should be unique`);
            }
            case 'P2003': {
                const regex = /Foreign key constraint failed on the field: (.+?)/;
                const match = regex.exec(exception.message);
                if (match)
                    throw new common_1.BadRequestException(`The field ${match[1]} is invalid`);
                break;
            }
            case 'P2025': {
                throw new common_1.NotFoundException(exception.message);
            }
            default:
                throw new common_1.BadRequestException('sorry, something went wrong');
        }
    }
};
exports.PrismaClientExceptionFilter = PrismaClientExceptionFilter;
exports.PrismaClientExceptionFilter = PrismaClientExceptionFilter = __decorate([
    (0, common_1.Catch)(client_1.Prisma.PrismaClientKnownRequestError)
], PrismaClientExceptionFilter);
//# sourceMappingURL=prisma-client-exception.filter.js.map