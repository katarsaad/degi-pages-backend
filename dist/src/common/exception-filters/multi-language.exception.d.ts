import { HttpException } from '@nestjs/common';
import { WsException } from '@nestjs/websockets';
export declare class MultiLanguageException extends HttpException {
    constructor(customCode: string);
}
export declare class MultiLanguageWsException extends WsException {
    constructor(customCode: string);
}
