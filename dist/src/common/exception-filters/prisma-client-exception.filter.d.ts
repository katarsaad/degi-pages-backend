import { BaseExceptionFilter } from '@nestjs/core';
import { Prisma } from '@prisma/client';
export declare class PrismaClientExceptionFilter extends BaseExceptionFilter {
    private readonly logger;
    catch(exception: Prisma.PrismaClientKnownRequestError): void;
}
