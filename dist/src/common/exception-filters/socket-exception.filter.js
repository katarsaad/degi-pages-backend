"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketExceptionFilter = void 0;
const common_1 = require("@nestjs/common");
const websockets_1 = require("@nestjs/websockets");
const header_language_enum_1 = require("../enums/header-language.enum");
const JSON_ERRORS = require("../../../errors.json");
let SocketExceptionFilter = class SocketExceptionFilter extends websockets_1.BaseWsExceptionFilter {
    constructor() {
        super();
        this.logger = new common_1.Logger('SocketException');
    }
    catch(exception, host) {
        const ACKCallback = host.getArgByIndex(2);
        if (ACKCallback && typeof ACKCallback === 'function') {
            const errorMessage = this.errorMessage(exception, host);
            this.logger.error(errorMessage?.message || 'Socket error');
            ACKCallback({ success: false, ...errorMessage });
        }
    }
    errorMessage(exception, host) {
        const req = host.switchToWs().getClient();
        const language = req.headers['accept-language'] || header_language_enum_1.HeaderLanguageEnum.EN;
        const response = exception.response;
        this.logger.error(exception);
        if (Array.isArray(exception?.error))
            return { message: exception.error[0], code: null };
        if (exception?.error)
            return { message: exception.error, code: null };
        if (!response)
            return { message: 'Socket error', code: null };
        const translatedError = this.WsErrorToThrow(response);
        if (translatedError && language === header_language_enum_1.HeaderLanguageEnum.EN)
            return { code: translatedError.code, message: translatedError?.multiLanguageError?.EN };
        if (translatedError && language === header_language_enum_1.HeaderLanguageEnum.AR)
            return { code: translatedError.code, message: translatedError?.multiLanguageError?.EN };
    }
    WsErrorToThrow(code) {
        const errorToReturn = JSON_ERRORS.filter((error) => {
            return error[code];
        });
        const error = errorToReturn[0][code];
        return { multiLanguageError: error, code };
    }
};
exports.SocketExceptionFilter = SocketExceptionFilter;
exports.SocketExceptionFilter = SocketExceptionFilter = __decorate([
    (0, common_1.Catch)(),
    __metadata("design:paramtypes", [])
], SocketExceptionFilter);
//# sourceMappingURL=socket-exception.filter.js.map