"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpExceptionFilter = void 0;
const common_1 = require("@nestjs/common");
const JSON_ERRORS = require("../../../errors.json");
const header_language_enum_1 = require("../enums/header-language.enum");
let HttpExceptionFilter = class HttpExceptionFilter {
    constructor() {
        this.logger = new common_1.Logger();
    }
    catch(exception, host) {
        let returnedMessage = '';
        let errorCode = null;
        const ctx = host.switchToHttp();
        const headerLang = ctx.getRequest().headers['accept-language'];
        const request = ctx.getRequest();
        const response = ctx.getResponse();
        const exceptionRes = exception.getResponse();
        const statusCode = exception instanceof common_1.HttpException ? exception.getStatus() : common_1.HttpStatus.INTERNAL_SERVER_ERROR;
        const message = exceptionRes.message;
        if (Array.isArray(message)) {
            returnedMessage = message[0];
        }
        else if (message) {
            returnedMessage = message;
        }
        else {
            const { multiLanguageError, code } = this.HttpErrorToThrow(exceptionRes);
            errorCode = code || null;
            switch (headerLang) {
                case header_language_enum_1.HeaderLanguageEnum.EN: {
                    returnedMessage = multiLanguageError.EN;
                    break;
                }
                case header_language_enum_1.HeaderLanguageEnum.AR: {
                    returnedMessage = multiLanguageError.AR;
                    break;
                }
                default:
                    if (multiLanguageError.EN)
                        returnedMessage = multiLanguageError.EN;
                    break;
            }
        }
        this.logger.error(`${new Date().toISOString()} - request method: ${request.method} request url${request.url} error:${returnedMessage}`);
        return response.status(statusCode).json({
            statusCode,
            timestamp: new Date().toISOString(),
            path: request.url,
            message: returnedMessage,
            errorCode,
        });
    }
    HttpErrorToThrow(code) {
        const errorToReturn = JSON_ERRORS.filter((error) => {
            return error[code];
        });
        const error = errorToReturn[0][code];
        return { multiLanguageError: error, code };
    }
};
exports.HttpExceptionFilter = HttpExceptionFilter;
exports.HttpExceptionFilter = HttpExceptionFilter = __decorate([
    (0, common_1.Catch)(common_1.HttpException),
    __metadata("design:paramtypes", [])
], HttpExceptionFilter);
//# sourceMappingURL=http-exception.filter.js.map