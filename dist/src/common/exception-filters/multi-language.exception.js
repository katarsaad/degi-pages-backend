"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MultiLanguageWsException = exports.MultiLanguageException = void 0;
const common_1 = require("@nestjs/common");
const JSON_ERRORS = require("../../../errors.json");
const websockets_1 = require("@nestjs/websockets");
class MultiLanguageException extends common_1.HttpException {
    constructor(customCode) {
        const [status, code] = customCode.split('.');
        super(code, +status);
    }
}
exports.MultiLanguageException = MultiLanguageException;
class MultiLanguageWsException extends websockets_1.WsException {
    constructor(customCode) {
        const [_, code] = customCode.split('.');
        const errorToThrow = JSON_ERRORS.filter((error) => {
            return error[code];
        });
        const error = errorToThrow[0][code];
        super(error);
    }
}
exports.MultiLanguageWsException = MultiLanguageWsException;
//# sourceMappingURL=multi-language.exception.js.map