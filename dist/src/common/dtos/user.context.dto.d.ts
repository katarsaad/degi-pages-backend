export declare class UserContext {
    id: number;
    name: string;
    email: string;
}
