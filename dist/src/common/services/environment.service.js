"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EnvironmentService = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
let EnvironmentService = class EnvironmentService {
    constructor(configService) {
        this.configService = configService;
    }
    get serverConfig() {
        return {
            serverEnv: this.configService.get('SERVER_ENV'),
            port: this.configService.get('PORT'),
            serverURL: this.configService.get('SERVER_URL'),
        };
    }
    get clientConfig() {
        return {
            frontURL: this.configService.get('FRONT_URL'),
        };
    }
    get dataBaseConfig() {
        return {
            dataBaseUser: this.configService.get('DATABASE_USER'),
            dataBasePassword: this.configService.get('DATABASE_PASSWORD'),
            dataBaseName: this.configService.get('DATABASE_NAME'),
            dataBasePort: this.configService.get('DATABASE_PORT'),
            dataBaseURL: this.configService.get('DATABASE_URL'),
        };
    }
    get jasonWebTokenConfig() {
        return {
            JWTSecretKey: this.configService.get('JWT_SECRET_KEY'),
            saltRounds: this.configService.get('SALT_ROUNDS'),
        };
    }
    get redisConfig() {
        return {
            host: this.configService.get('REDIS_HOST'),
            port: this.configService.get('REDIS_PORT'),
            password: this.configService.get('REDIS_PASSWORD'),
            tls: this.configService.get('REDIS_TLS'),
            TTL: this.configService.get('REDIS_TTL'),
            longTimeToLive: this.configService.get('REDIS_LONG_TTL'),
            longerTimeToLive: this.configService.get('REDIS_LONGER_TTL'),
        };
    }
};
exports.EnvironmentService = EnvironmentService;
exports.EnvironmentService = EnvironmentService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [config_1.ConfigService])
], EnvironmentService);
//# sourceMappingURL=environment.service.js.map