import { ConfigService } from '@nestjs/config';
export declare class EnvironmentService {
    private readonly configService;
    constructor(configService: ConfigService);
    get serverConfig(): {
        serverEnv: string;
        port: number;
        serverURL: number;
    };
    get clientConfig(): {
        frontURL: string;
    };
    get dataBaseConfig(): {
        dataBaseUser: string;
        dataBasePassword: string;
        dataBaseName: string;
        dataBasePort: string;
        dataBaseURL: string;
    };
    get jasonWebTokenConfig(): {
        JWTSecretKey: string;
        saltRounds: number;
    };
    get redisConfig(): {
        host: string;
        port: number;
        password: string;
        tls: boolean;
        TTL: number;
        longTimeToLive: number;
        longerTimeToLive: number;
    };
}
