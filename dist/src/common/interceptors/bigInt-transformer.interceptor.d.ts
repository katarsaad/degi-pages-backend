import { CallHandler, ExecutionContext, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
export interface Response<T> {
    data: T;
}
export declare class BigIntTransformerInterceptor<T> implements NestInterceptor<T, Response<T>> {
    intercept(_: ExecutionContext, next: CallHandler): Observable<Response<T>>;
}
