"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LanguageErrorEnum = void 0;
var LanguageErrorEnum;
(function (LanguageErrorEnum) {
    LanguageErrorEnum["EXTRA_NOT_FOUND"] = "404.1";
    LanguageErrorEnum["NO_EXTRAS_FOUND"] = "404.2";
    LanguageErrorEnum["EXTRA_UPDATE_FAILED"] = "400.3";
    LanguageErrorEnum["EXTRA_CREATION_FAILED"] = "400.4";
    LanguageErrorEnum["EXTRA_DELETION_FAILED"] = "400.5";
    LanguageErrorEnum["MANAGER_NOT_FOUND"] = "404.6";
    LanguageErrorEnum["CREATION_FAILED"] = "400.7";
    LanguageErrorEnum["UPDATE_FAILED"] = "400.8";
    LanguageErrorEnum["DELETION_FAILED"] = "400.9";
    LanguageErrorEnum["INVALID_PASSWORD"] = "400.10";
    LanguageErrorEnum["MANAGER_EXIST"] = "400.11";
})(LanguageErrorEnum || (exports.LanguageErrorEnum = LanguageErrorEnum = {}));
//# sourceMappingURL=language-error.enum.js.map