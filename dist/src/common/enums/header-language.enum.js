"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HeaderLanguageEnum = void 0;
var HeaderLanguageEnum;
(function (HeaderLanguageEnum) {
    HeaderLanguageEnum["EN"] = "en";
    HeaderLanguageEnum["AR"] = "ar";
})(HeaderLanguageEnum || (exports.HeaderLanguageEnum = HeaderLanguageEnum = {}));
//# sourceMappingURL=header-language.enum.js.map