"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JwtAuth = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const jwt_guard_1 = require("../jwt/jwt.guard");
function JwtAuth() {
    return (0, common_1.applyDecorators)((0, swagger_1.ApiBearerAuth)(), (0, swagger_1.ApiUnauthorizedResponse)(), (0, common_1.UseGuards)(jwt_guard_1.JwtAuthGuard));
}
exports.JwtAuth = JwtAuth;
//# sourceMappingURL=jwt.auth.decorator.js.map