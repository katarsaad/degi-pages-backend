import { RegisterDto } from './dto/register.dto';
import { ManagerAuthResponse } from './responses/manager.auth.response';
import { AuthService } from './auth.service';
import { TokenResponse } from './responses/token.response';
import { LoginDto } from './dto/login.dto';
export declare class AuthController {
    private readonly authService;
    constructor(authService: AuthService);
    register(registerDto: RegisterDto): Promise<ManagerAuthResponse>;
    login(dto: LoginDto): Promise<TokenResponse>;
}
