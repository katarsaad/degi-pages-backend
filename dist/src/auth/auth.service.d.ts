import { PrismaService } from 'src/common/services/prisma.service';
import { JwtService } from '@nestjs/jwt';
import { EnvironmentService } from 'src/common/services/environment.service';
import { TokenResponse } from './responses/token.response';
import { ManagerAuthResponse } from './responses/manager.auth.response';
import { RegisterDto } from './dto/register.dto';
export declare class AuthService {
    private prisma;
    private jwtService;
    private environmentService;
    constructor(prisma: PrismaService, jwtService: JwtService, environmentService: EnvironmentService);
    register(registerDto: RegisterDto): Promise<ManagerAuthResponse>;
    login(email: string, password: string): Promise<TokenResponse>;
    private hashPassword;
    private comparePassword;
    generateBearerToken(userId: number, expiresIn?: string | number): Promise<string>;
}
