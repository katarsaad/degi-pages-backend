"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../common/services/prisma.service");
const jwt_1 = require("@nestjs/jwt");
const environment_service_1 = require("../common/services/environment.service");
const multi_language_exception_1 = require("../common/exception-filters/multi-language.exception");
const language_error_enum_1 = require("../common/enums/language-error.enum");
let AuthService = class AuthService {
    constructor(prisma, jwtService, environmentService) {
        this.prisma = prisma;
        this.jwtService = jwtService;
        this.environmentService = environmentService;
    }
    async register(registerDto) {
        const { password, email, name } = registerDto;
        const manager = await this.prisma.manager.findFirst({ where: { email } });
        if (manager) {
            throw new multi_language_exception_1.MultiLanguageException(language_error_enum_1.LanguageErrorEnum.MANAGER_EXIST);
        }
        const hashedPassword = await this.hashPassword(password);
        try {
            const manager = await this.prisma.manager.create({
                data: {
                    email,
                    name: name,
                    password: hashedPassword,
                },
            });
            const bearerToken = await this.generateBearerToken(Number(manager.id));
            return { manager, bearerToken };
        }
        catch (error) {
            throw new multi_language_exception_1.MultiLanguageException(language_error_enum_1.LanguageErrorEnum.CREATION_FAILED);
        }
    }
    async login(email, password) {
        const manager = await this.prisma.manager.findUnique({
            where: {
                email,
            },
        });
        if (!manager) {
            throw new multi_language_exception_1.MultiLanguageException(language_error_enum_1.LanguageErrorEnum.MANAGER_NOT_FOUND);
        }
        const isPasswordValid = await this.comparePassword(password, manager.password);
        if (!isPasswordValid) {
            throw new multi_language_exception_1.MultiLanguageException(language_error_enum_1.LanguageErrorEnum.INVALID_PASSWORD);
        }
        const bearerToken = await this.generateBearerToken(manager.id);
        return { bearerToken };
    }
    async hashPassword(password) {
        return password;
    }
    async comparePassword(plainPassword, hashedPassword) {
        return plainPassword === hashedPassword;
    }
    async generateBearerToken(userId, expiresIn) {
        const payload = { sub: userId };
        return await this.jwtService.signAsync(payload, {
            secret: this.environmentService.jasonWebTokenConfig.JWTSecretKey,
            expiresIn: expiresIn ?? '10 days',
        });
    }
};
exports.AuthService = AuthService;
exports.AuthService = AuthService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService,
        jwt_1.JwtService,
        environment_service_1.EnvironmentService])
], AuthService);
//# sourceMappingURL=auth.service.js.map