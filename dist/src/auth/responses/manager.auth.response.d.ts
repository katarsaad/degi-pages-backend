import { ManagerResponse } from 'src/features/manager/responses/manager.response';
export declare class ManagerAuthResponse {
    manager: ManagerResponse;
    bearerToken: string;
}
