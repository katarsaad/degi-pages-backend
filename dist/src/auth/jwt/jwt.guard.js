"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.JwtAuthGuard = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const passport_1 = require("@nestjs/passport");
const jwt_1 = require("@nestjs/jwt");
const prisma_service_1 = require("../../common/services/prisma.service");
let JwtAuthGuard = class JwtAuthGuard extends (0, passport_1.AuthGuard)('jwt') {
    constructor(reflector, jwtService, prismaService) {
        super();
        this.reflector = reflector;
        this.jwtService = jwtService;
        this.prismaService = prismaService;
    }
    async canActivate(context) {
        const isPublic = this.reflector.getAllAndOverride('isPublic', [context.getHandler(), context.getClass()]);
        if (isPublic) {
            return true;
        }
        const request = this.getRequest(context);
        if (!request.headers) {
            throw new common_1.UnauthorizedException('Headers not found');
        }
        const token = this.extractTokenFromHeader(request);
        if (!token) {
            throw new common_1.UnauthorizedException('Token not found');
        }
        const isTokenValid = this.validateToken(token);
        if (!isTokenValid) {
            throw new common_1.UnauthorizedException('Token is not valid');
        }
        const decodedToken = this.jwtService.decode(token);
        if (!decodedToken) {
            throw new common_1.UnauthorizedException('Invalid token');
        }
        const manager = await this.prismaService.manager.findUnique({
            where: { id: decodedToken.sub },
            select: { id: true, email: true, name: true },
        });
        if (!manager) {
            throw new common_1.ForbiddenException('Manager not found');
        }
        request.user = { name: manager.name, email: manager.email, id: manager.id };
        return this.handleRequest(null, manager, null, context);
    }
    getRequest(context) {
        const httpContext = context.switchToHttp();
        const request = httpContext.getRequest();
        return request;
    }
    extractTokenFromHeader(request) {
        const authHeader = request.headers['authorization'];
        if (!authHeader) {
            return null;
        }
        const parts = authHeader.split(' ');
        if (parts.length !== 2 || parts[0] !== 'Bearer') {
            return null;
        }
        return parts[1];
    }
    validateToken(token) {
        try {
            const decoded = this.jwtService.verify(token, { secret: process.env.JWT_SECRET_KEY });
            return !!decoded;
        }
        catch (error) {
            console.error('Token validation error:', error);
            return false;
        }
    }
};
exports.JwtAuthGuard = JwtAuthGuard;
exports.JwtAuthGuard = JwtAuthGuard = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [core_1.Reflector,
        jwt_1.JwtService,
        prisma_service_1.PrismaService])
], JwtAuthGuard);
//# sourceMappingURL=jwt.guard.js.map