import { CurrencyEnum } from '@prisma/client';
export declare class ExtraResponse {
    id: bigint;
    name: string;
    price: number;
    currency: CurrencyEnum;
}
