import { PrismaService } from 'src/common/services/prisma.service';
import { CreateExtraDto } from './dtos/create.extra.dto';
import { UpdateExtraDto } from './dtos/update.extra.dto';
import { ExtraResponse } from './responses/extra.response';
export declare class ExtrasService {
    private readonly prisma;
    constructor(prisma: PrismaService);
    getById(id: bigint): Promise<ExtraResponse>;
    getAll(): Promise<ExtraResponse[]>;
    update(id: bigint, data: UpdateExtraDto): Promise<ExtraResponse>;
    create(data: CreateExtraDto): Promise<ExtraResponse>;
    delete(id: bigint): Promise<void>;
    private transformExtra;
}
