"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExtrasService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../common/services/prisma.service");
const multi_language_exception_1 = require("../common/exception-filters/multi-language.exception");
const language_error_enum_1 = require("../common/enums/language-error.enum");
let ExtrasService = class ExtrasService {
    constructor(prisma) {
        this.prisma = prisma;
    }
    async getById(id) {
        const extra = await this.prisma.extra.findUnique({ where: { id: Number(id) } });
        if (!extra) {
            throw new multi_language_exception_1.MultiLanguageException(language_error_enum_1.LanguageErrorEnum.EXTRA_NOT_FOUND);
        }
        return this.transformExtra(extra);
    }
    async getAll() {
        const extras = await this.prisma.extra.findMany();
        if (extras.length === 0) {
            throw new multi_language_exception_1.MultiLanguageException(language_error_enum_1.LanguageErrorEnum.NO_EXTRAS_FOUND);
        }
        return extras.map(this.transformExtra);
    }
    async update(id, data) {
        const existingExtra = await this.getById(id);
        const updatedExtra = await this.prisma.extra.update({
            where: { id: Number(existingExtra.id) },
            data,
        });
        if (!updatedExtra) {
            throw new multi_language_exception_1.MultiLanguageException(language_error_enum_1.LanguageErrorEnum.EXTRA_UPDATE_FAILED);
        }
        return this.transformExtra(updatedExtra);
    }
    async create(data) {
        const createdExtra = await this.prisma.extra.create({ data });
        if (!createdExtra) {
            throw new multi_language_exception_1.MultiLanguageException(language_error_enum_1.LanguageErrorEnum.EXTRA_CREATION_FAILED);
        }
        return this.transformExtra(createdExtra);
    }
    async delete(id) {
        const existingExtra = await this.getById(id);
        const deletedExtra = await this.prisma.extra.delete({ where: { id: Number(existingExtra.id) } });
        if (!deletedExtra) {
            throw new multi_language_exception_1.MultiLanguageException(language_error_enum_1.LanguageErrorEnum.EXTRA_DELETION_FAILED);
        }
    }
    transformExtra(extra) {
        return {
            id: extra.id,
            name: extra.name,
            price: extra.price,
            currency: extra.currency,
        };
    }
};
exports.ExtrasService = ExtrasService;
exports.ExtrasService = ExtrasService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], ExtrasService);
//# sourceMappingURL=extras.service.js.map