"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExtrasModule = void 0;
const common_1 = require("@nestjs/common");
const extras_service_1 = require("./extras.service");
const extras_controller_1 = require("./extras.controller");
const common_module_1 = require("../common/common.module");
let ExtrasModule = class ExtrasModule {
};
exports.ExtrasModule = ExtrasModule;
exports.ExtrasModule = ExtrasModule = __decorate([
    (0, common_1.Module)({
        imports: [common_module_1.CommonModule],
        exports: [extras_service_1.ExtrasService],
        providers: [extras_service_1.ExtrasService],
        controllers: [extras_controller_1.ExtrasController],
    })
], ExtrasModule);
//# sourceMappingURL=extras.module.js.map