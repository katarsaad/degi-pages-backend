export declare class CreateExtraDto {
    name: string;
    price: number;
    currency: 'TND' | 'EURO' | 'DOLAR';
}
