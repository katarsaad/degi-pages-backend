import { CurrencyEnum } from '@prisma/client';
export declare class UpdateExtraDto {
    name?: string;
    price?: number;
    currency?: CurrencyEnum;
}
