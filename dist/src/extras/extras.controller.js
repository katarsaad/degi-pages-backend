"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExtrasController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const extras_service_1 = require("./extras.service");
const create_extra_dto_1 = require("./dtos/create.extra.dto");
const update_extra_dto_1 = require("./dtos/update.extra.dto");
const extra_response_1 = require("./responses/extra.response");
const jwt_auth_decorator_1 = require("../auth/decorators/jwt.auth.decorator");
let ExtrasController = class ExtrasController {
    constructor(extrasService) {
        this.extrasService = extrasService;
    }
    getAll() {
        return this.extrasService.getAll();
    }
    getById(id) {
        return this.extrasService.getById(id);
    }
    update(id, data) {
        return this.extrasService.update(id, data);
    }
    create(data) {
        return this.extrasService.create(data);
    }
    delete(id) {
        return this.extrasService.delete(id);
    }
};
exports.ExtrasController = ExtrasController;
__decorate([
    (0, jwt_auth_decorator_1.JwtAuth)(),
    (0, common_1.Get)(),
    (0, swagger_1.ApiOperation)({ summary: 'Retrieve all extras' }),
    (0, swagger_1.ApiResponse)({ status: 200, description: 'Returns a list of all extras.', type: [extra_response_1.ExtraResponse] }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ExtrasController.prototype, "getAll", null);
__decorate([
    (0, jwt_auth_decorator_1.JwtAuth)(),
    (0, common_1.Get)(':id'),
    (0, swagger_1.ApiOperation)({ summary: 'Retrieve an extra by ID' }),
    (0, swagger_1.ApiResponse)({ status: 200, description: 'Returns the extra with the given ID.', type: extra_response_1.ExtraResponse }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [BigInt]),
    __metadata("design:returntype", Promise)
], ExtrasController.prototype, "getById", null);
__decorate([
    (0, jwt_auth_decorator_1.JwtAuth)(),
    (0, common_1.Put)(':id'),
    (0, swagger_1.ApiOperation)({ summary: 'Update an extra by ID' }),
    (0, swagger_1.ApiResponse)({ status: 200, description: 'Updates the extra with the given ID.' }),
    (0, swagger_1.ApiBody)({ type: update_extra_dto_1.UpdateExtraDto }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [BigInt, update_extra_dto_1.UpdateExtraDto]),
    __metadata("design:returntype", Promise)
], ExtrasController.prototype, "update", null);
__decorate([
    (0, jwt_auth_decorator_1.JwtAuth)(),
    (0, common_1.Post)(),
    (0, swagger_1.ApiOperation)({ summary: 'Create a new extra' }),
    (0, swagger_1.ApiResponse)({ status: 201, description: 'Creates a new extra and returns it.' }),
    (0, swagger_1.ApiBody)({ type: create_extra_dto_1.CreateExtraDto }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_extra_dto_1.CreateExtraDto]),
    __metadata("design:returntype", Promise)
], ExtrasController.prototype, "create", null);
__decorate([
    (0, jwt_auth_decorator_1.JwtAuth)(),
    (0, common_1.Delete)(':id'),
    (0, swagger_1.ApiOperation)({ summary: 'Delete an extra by ID' }),
    (0, swagger_1.ApiResponse)({ status: 204, description: 'Deletes the extra with the given ID.' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [BigInt]),
    __metadata("design:returntype", Promise)
], ExtrasController.prototype, "delete", null);
exports.ExtrasController = ExtrasController = __decorate([
    (0, swagger_1.ApiTags)('extras'),
    (0, common_1.Controller)('extras'),
    __metadata("design:paramtypes", [extras_service_1.ExtrasService])
], ExtrasController);
//# sourceMappingURL=extras.controller.js.map