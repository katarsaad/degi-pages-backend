import { ExtrasService } from './extras.service';
import { CreateExtraDto } from './dtos/create.extra.dto';
import { UpdateExtraDto } from './dtos/update.extra.dto';
import { ExtraResponse } from './responses/extra.response';
export declare class ExtrasController {
    private readonly extrasService;
    constructor(extrasService: ExtrasService);
    getAll(): Promise<ExtraResponse[]>;
    getById(id: bigint): Promise<ExtraResponse>;
    update(id: bigint, data: UpdateExtraDto): Promise<ExtraResponse>;
    create(data: CreateExtraDto): Promise<ExtraResponse>;
    delete(id: bigint): Promise<void>;
}
