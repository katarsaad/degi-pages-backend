import { PrismaService } from 'src/common/services/prisma.service';
import { CreateManagerDto } from './dto/create.manager.dto';
import { UpdateManagerDto } from './dto/update.manager.dto';
import { ManagerResponse } from './responses/manager.response';
export declare class ManagerService {
    readonly prisma: PrismaService;
    constructor(prisma: PrismaService);
    createManager(managerData: CreateManagerDto): Promise<ManagerResponse>;
    findAllManagers(): Promise<ManagerResponse[]>;
    findOneManager(id: number): Promise<ManagerResponse | null>;
    updateManager(id: number, managerData: UpdateManagerDto): Promise<ManagerResponse>;
    deleteManager(id: number): Promise<ManagerResponse>;
}
