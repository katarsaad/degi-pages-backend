"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ManagerController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const manager_service_1 = require("./manager.service");
const create_manager_dto_1 = require("./dto/create.manager.dto");
const update_manager_dto_1 = require("./dto/update.manager.dto");
const manager_response_1 = require("./responses/manager.response");
const jwt_guard_1 = require("../../auth/jwt/jwt.guard");
const public_decorator_1 = require("../../auth/decorators/public.decorator");
const jwt_auth_decorator_1 = require("../../auth/decorators/jwt.auth.decorator");
let ManagerController = class ManagerController {
    constructor(managerService) {
        this.managerService = managerService;
    }
    async createManager(createManagerDto) {
        return this.managerService.createManager(createManagerDto);
    }
    async findAllManagers() {
        return this.managerService.findAllManagers();
    }
    async findOneManager(id) {
        return this.managerService.findOneManager(id);
    }
    async updateManager(id, updateManagerDto) {
        return this.managerService.updateManager(id, updateManagerDto);
    }
    async deleteManager(id) {
        return this.managerService.deleteManager(id);
    }
};
exports.ManagerController = ManagerController;
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiOperation)({ summary: 'Create a new manager' }),
    (0, swagger_1.ApiResponse)({ status: 201, description: 'The manager has been successfully created.' }),
    (0, swagger_1.ApiResponse)({ status: 400, description: 'Bad Request.' }),
    (0, swagger_1.ApiBody)({ type: create_manager_dto_1.CreateManagerDto }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_manager_dto_1.CreateManagerDto]),
    __metadata("design:returntype", Promise)
], ManagerController.prototype, "createManager", null);
__decorate([
    (0, jwt_auth_decorator_1.JwtAuth)(),
    (0, common_1.Get)(),
    (0, swagger_1.ApiOperation)({ summary: 'Find all managers' }),
    (0, swagger_1.ApiResponse)({ status: 200, description: 'Successfully retrieved all managers.' }),
    (0, swagger_1.ApiResponse)({ status: 500, description: 'Internal Server Error.' }),
    (0, common_1.UseInterceptors)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ManagerController.prototype, "findAllManagers", null);
__decorate([
    (0, common_1.Get)(':id'),
    (0, swagger_1.ApiOperation)({ summary: 'Find a manager by ID' }),
    (0, swagger_1.ApiResponse)({ status: 200, description: 'Successfully retrieved the manager.' }),
    (0, swagger_1.ApiResponse)({ status: 404, description: 'Manager not found.' }),
    (0, common_1.UseInterceptors)(),
    __param(0, (0, common_1.Param)('id', common_1.ParseIntPipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ManagerController.prototype, "findOneManager", null);
__decorate([
    (0, public_decorator_1.IsPublic)(),
    (0, common_1.UseGuards)(jwt_guard_1.JwtAuthGuard),
    (0, common_1.Put)(':id'),
    (0, swagger_1.ApiOperation)({ summary: 'Update a manager' }),
    (0, swagger_1.ApiResponse)({ status: 200, description: 'Successfully updated the manager.' }),
    (0, swagger_1.ApiResponse)({ status: 400, description: 'Bad Request.', type: manager_response_1.ManagerResponse }),
    (0, swagger_1.ApiBody)({ type: update_manager_dto_1.UpdateManagerDto }),
    (0, common_1.UseInterceptors)(),
    __param(0, (0, common_1.Param)('id', common_1.ParseIntPipe)),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, update_manager_dto_1.UpdateManagerDto]),
    __metadata("design:returntype", Promise)
], ManagerController.prototype, "updateManager", null);
__decorate([
    (0, common_1.Delete)(':id'),
    (0, swagger_1.ApiOperation)({ summary: 'Delete a manager' }),
    (0, swagger_1.ApiResponse)({ status: 200, description: 'Successfully deleted the manager.' }),
    (0, swagger_1.ApiResponse)({ status: 404, description: 'Manager not found.' }),
    (0, common_1.UseInterceptors)(),
    __param(0, (0, common_1.Param)('id', common_1.ParseIntPipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ManagerController.prototype, "deleteManager", null);
exports.ManagerController = ManagerController = __decorate([
    (0, swagger_1.ApiTags)('managers'),
    (0, common_1.Controller)('managers'),
    (0, common_1.UseInterceptors)(),
    __metadata("design:paramtypes", [manager_service_1.ManagerService])
], ManagerController);
//# sourceMappingURL=manager.controller.js.map