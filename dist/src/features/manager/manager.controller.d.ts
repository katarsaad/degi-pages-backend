import { ManagerService } from './manager.service';
import { CreateManagerDto } from './dto/create.manager.dto';
import { UpdateManagerDto } from './dto/update.manager.dto';
import { ManagerResponse } from './responses/manager.response';
export declare class ManagerController {
    private readonly managerService;
    constructor(managerService: ManagerService);
    createManager(createManagerDto: CreateManagerDto): Promise<ManagerResponse>;
    findAllManagers(): Promise<ManagerResponse[]>;
    findOneManager(id: number): Promise<ManagerResponse | null>;
    updateManager(id: number, updateManagerDto: UpdateManagerDto): Promise<ManagerResponse>;
    deleteManager(id: number): Promise<ManagerResponse>;
}
