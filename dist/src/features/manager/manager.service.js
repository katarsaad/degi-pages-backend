"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ManagerService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../../common/services/prisma.service");
const language_error_enum_1 = require("../../common/enums/language-error.enum");
const multi_language_exception_1 = require("../../common/exception-filters/multi-language.exception");
let ManagerService = class ManagerService {
    constructor(prisma) {
        this.prisma = prisma;
    }
    async createManager(managerData) {
        const existingManager = await this.prisma.manager.findUnique({ where: { email: managerData.email } });
        if (existingManager) {
            throw new multi_language_exception_1.MultiLanguageException(language_error_enum_1.LanguageErrorEnum.CREATION_FAILED);
        }
        return this.prisma.manager.create({ data: managerData });
    }
    async findAllManagers() {
        return this.prisma.manager.findMany();
    }
    async findOneManager(id) {
        const manager = await this.prisma.manager.findUnique({ where: { id } });
        if (!manager) {
            throw new multi_language_exception_1.MultiLanguageException(language_error_enum_1.LanguageErrorEnum.MANAGER_NOT_FOUND);
        }
        return manager;
    }
    async updateManager(id, managerData) {
        const manager = await this.prisma.manager.findUnique({ where: { id } });
        if (!manager) {
            throw new multi_language_exception_1.MultiLanguageException(language_error_enum_1.LanguageErrorEnum.MANAGER_NOT_FOUND);
        }
        return this.prisma.manager.update({ where: { id }, data: managerData });
    }
    async deleteManager(id) {
        const manager = await this.prisma.manager.findUnique({ where: { id } });
        if (!manager) {
            throw new multi_language_exception_1.MultiLanguageException(language_error_enum_1.LanguageErrorEnum.MANAGER_NOT_FOUND);
        }
        return this.prisma.manager.delete({ where: { id } });
    }
};
exports.ManagerService = ManagerService;
exports.ManagerService = ManagerService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], ManagerService);
//# sourceMappingURL=manager.service.js.map