export declare class CreateManagerDto {
    email: string;
    name?: string;
    password: string;
}
