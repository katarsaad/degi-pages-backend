"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.initGlobalConfig = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const http_exception_filter_1 = require("../common/exception-filters/http-exception.filter");
const prisma_client_exception_filter_1 = require("../common/exception-filters/prisma-client-exception.filter");
const bigInt_transformer_interceptor_1 = require("../common/interceptors/bigInt-transformer.interceptor");
function initGlobalConfig(app) {
    const { httpAdapter } = app.get(core_1.HttpAdapterHost);
    app.useGlobalFilters(new http_exception_filter_1.HttpExceptionFilter(), new prisma_client_exception_filter_1.PrismaClientExceptionFilter(httpAdapter));
    app.useGlobalInterceptors(new bigInt_transformer_interceptor_1.BigIntTransformerInterceptor());
    app.useGlobalPipes(new common_1.ValidationPipe({
        whitelist: true,
        forbidNonWhitelisted: true,
        transform: true,
    }));
}
exports.initGlobalConfig = initGlobalConfig;
//# sourceMappingURL=global.config.js.map