"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.initSwaggerConfig = void 0;
const swagger_1 = require("@nestjs/swagger");
function initSwaggerConfig(app) {
    const config = new swagger_1.DocumentBuilder()
        .setTitle('Smoking Cessation API')
        .setDescription('Smoking Cessation Backend API description')
        .setVersion('1.0')
        .addBearerAuth()
        .addApiKey({
        type: 'apiKey',
        name: 'Accept-Language',
        in: 'header',
    }, 'Accept-Language')
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, config);
    swagger_1.SwaggerModule.setup('api-docs', app, document);
}
exports.initSwaggerConfig = initSwaggerConfig;
//# sourceMappingURL=swagger.cofig.js.map