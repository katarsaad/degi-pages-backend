"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./features/app.module");
const swagger_cofig_1 = require("./config/swagger.cofig");
const global_config_1 = require("./config/global.config");
const dotenv = require("dotenv");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    dotenv.config();
    (0, swagger_cofig_1.initSwaggerConfig)(app);
    (0, global_config_1.initGlobalConfig)(app);
    await app.listen(3000);
}
bootstrap();
//# sourceMappingURL=main.js.map