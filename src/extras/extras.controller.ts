import { Controller, Get, Post, Put, Delete, Param, Body } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse, ApiBody } from '@nestjs/swagger';
import { ExtrasService } from './extras.service';
import { CreateExtraDto } from './dtos/create.extra.dto';
import { UpdateExtraDto } from './dtos/update.extra.dto';
import { ExtraResponse } from './responses/extra.response';
import { JwtAuth } from 'src/auth/decorators/jwt.auth.decorator';

@ApiTags('extras')
@Controller('extras')
export class ExtrasController {
  constructor(private readonly extrasService: ExtrasService) {}

  @JwtAuth()
  @Get()
  @ApiOperation({ summary: 'Retrieve all extras' })
  @ApiResponse({ status: 200, description: 'Returns a list of all extras.', type: [ExtraResponse] })
  getAll(): Promise<ExtraResponse[]> {
    return this.extrasService.getAll();
  }

  @JwtAuth()
  @Get(':id')
  @ApiOperation({ summary: 'Retrieve an extra by ID' })
  @ApiResponse({ status: 200, description: 'Returns the extra with the given ID.', type: ExtraResponse })
  getById(@Param('id') id: bigint): Promise<ExtraResponse> {
    return this.extrasService.getById(id);
  }

  @JwtAuth()
  @Put(':id')
  @ApiOperation({ summary: 'Update an extra by ID' })
  @ApiResponse({ status: 200, description: 'Updates the extra with the given ID.' })
  @ApiBody({ type: UpdateExtraDto })
  update(@Param('id') id: bigint, @Body() data: UpdateExtraDto): Promise<ExtraResponse> {
    return this.extrasService.update(id, data);
  }

  @JwtAuth()
  @Post()
  @ApiOperation({ summary: 'Create a new extra' })
  @ApiResponse({ status: 201, description: 'Creates a new extra and returns it.' })
  @ApiBody({ type: CreateExtraDto })
  create(@Body() data: CreateExtraDto): Promise<ExtraResponse> {
    return this.extrasService.create(data);
  }
  @JwtAuth()
  @Delete(':id')
  @ApiOperation({ summary: 'Delete an extra by ID' })
  @ApiResponse({ status: 204, description: 'Deletes the extra with the given ID.' })
  delete(@Param('id') id: bigint): Promise<void> {
    return this.extrasService.delete(id);
  }
}
