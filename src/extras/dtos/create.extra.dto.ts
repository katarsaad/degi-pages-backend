import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateExtraDto {
  @IsNotEmpty()
  @ApiProperty({ description: 'Name of the extra' })
  name: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'Price of the extra' })
  price: number;

  @IsNotEmpty()
  @ApiProperty({ enum: ['TND', 'EURO', 'DOLAR'], description: 'Currency of the extra' })
  currency: 'TND' | 'EURO' | 'DOLAR';
}
