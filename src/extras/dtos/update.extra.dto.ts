import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { CurrencyEnum } from '@prisma/client';

export class UpdateExtraDto {
  @IsNotEmpty()
  @ApiProperty({ description: 'Name of the extra' })
  name?: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'Price of the extra' })
  price?: number;

  @IsNotEmpty()
  @ApiProperty({ enum: ['TND', 'EURO', 'DOLAR'], description: 'Currency of the extra' })
  currency?: CurrencyEnum;
}
