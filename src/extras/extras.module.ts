import { Module } from '@nestjs/common';
import { ExtrasService } from './extras.service';
import { ExtrasController } from './extras.controller';
import { CommonModule } from 'src/common/common.module';

@Module({
  imports: [CommonModule],
  exports: [ExtrasService],
  providers: [ExtrasService],
  controllers: [ExtrasController],
})
export class ExtrasModule {}
