// src/extras/extras.service.ts
import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/common/services/prisma.service';
import { CreateExtraDto } from './dtos/create.extra.dto';
import { UpdateExtraDto } from './dtos/update.extra.dto';
import { ExtraResponse } from './responses/extra.response';
import { MultiLanguageException } from 'src/common/exception-filters/multi-language.exception';
import { LanguageErrorEnum } from 'src/common/enums/language-error.enum';
import { Extra } from '@prisma/client';

@Injectable()
export class ExtrasService {
  constructor(private readonly prisma: PrismaService) {}

  async getById(id: bigint): Promise<ExtraResponse> {
    const extra = await this.prisma.extra.findUnique({ where: { id: Number(id) } });
    if (!extra) {
      throw new MultiLanguageException(LanguageErrorEnum.EXTRA_NOT_FOUND);
    }
    return this.transformExtra(extra);
  }

  async getAll(): Promise<ExtraResponse[]> {
    const extras = await this.prisma.extra.findMany();
    if (extras.length === 0) {
      throw new MultiLanguageException(LanguageErrorEnum.NO_EXTRAS_FOUND);
    }
    return extras.map(this.transformExtra);
  }

  async update(id: bigint, data: UpdateExtraDto): Promise<ExtraResponse> {
    const existingExtra = await this.getById(id);
    const updatedExtra = await this.prisma.extra.update({
      where: { id: Number(existingExtra.id) },
      data,
    });
    if (!updatedExtra) {
      throw new MultiLanguageException(LanguageErrorEnum.EXTRA_UPDATE_FAILED);
    }
    return this.transformExtra(updatedExtra);
  }

  async create(data: CreateExtraDto): Promise<ExtraResponse> {
    const createdExtra = await this.prisma.extra.create({ data });
    if (!createdExtra) {
      throw new MultiLanguageException(LanguageErrorEnum.EXTRA_CREATION_FAILED);
    }
    return this.transformExtra(createdExtra);
  }

  async delete(id: bigint): Promise<void> {
    const existingExtra = await this.getById(id);

    const deletedExtra = await this.prisma.extra.delete({ where: { id: Number(existingExtra.id) } });
    if (!deletedExtra) {
      throw new MultiLanguageException(LanguageErrorEnum.EXTRA_DELETION_FAILED);
    }
  }

  private transformExtra(extra: Extra): ExtraResponse {
    return {
      id: extra.id,
      name: extra.name,
      price: extra.price,
      currency: extra.currency,
    };
  }
}
