import { ApiProperty } from '@nestjs/swagger';
import { CurrencyEnum } from '@prisma/client';
import { IsNumber, IsString } from 'class-validator';

export class ExtraResponse {
  @ApiProperty({ description: 'The unique identifier of the extra' })
  @IsNumber()
  id: bigint;

  @ApiProperty({ description: 'The name of the extra' })
  @IsString()
  name: string;

  @ApiProperty({ description: 'The price of the extra' })
  @IsNumber()
  price: number;

  @ApiProperty({ description: 'The currency of the extra' })
  @IsString()
  currency: CurrencyEnum;
}
