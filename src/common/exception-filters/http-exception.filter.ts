import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus, Logger } from '@nestjs/common';
import * as JSON_ERRORS from '../../../errors.json';
import { HeaderLanguageEnum } from 'src/common/enums/header-language.enum';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  private readonly logger: Logger;
  constructor() {
    this.logger = new Logger();
  }
  catch(exception: HttpException, host: ArgumentsHost) {
    let returnedMessage = '';
    let errorCode = null;
    const ctx = host.switchToHttp();
    const headerLang = ctx.getRequest().headers['accept-language'];

    const request = ctx.getRequest();
    const response = ctx.getResponse();
    const exceptionRes = exception.getResponse();

    const statusCode = exception instanceof HttpException ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR; //exception instanceof MultiLanguageException

    const message = (exceptionRes as any).message;

    if (Array.isArray(message)) {
      returnedMessage = message[0];
    } else if (message) {
      returnedMessage = message;
    } else {
      const { multiLanguageError, code } = this.HttpErrorToThrow(exceptionRes as any);
      errorCode = code || null;

      switch (headerLang) {
        case HeaderLanguageEnum.EN: {
          returnedMessage = multiLanguageError.EN;
          break;
        }
        case HeaderLanguageEnum.AR: {
          returnedMessage = multiLanguageError.AR;
          break;
        }
        default:
          if (multiLanguageError.EN) returnedMessage = multiLanguageError.EN;
          break;
      }
    }

    this.logger.error(`${new Date().toISOString()} - request method: ${request.method} request url${request.url} error:${returnedMessage}`);

    return response.status(statusCode).json({
      statusCode,
      timestamp: new Date().toISOString(),
      path: request.url,
      message: returnedMessage,
      errorCode,
    });
  }

  HttpErrorToThrow(code: string) {
    const errorToReturn = JSON_ERRORS.filter((error) => {
      return error[code];
    });
    const error = errorToReturn[0][code];

    return { multiLanguageError: error, code };
  }
}
