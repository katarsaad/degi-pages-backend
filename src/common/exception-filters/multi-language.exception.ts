import { HttpException } from '@nestjs/common';
import * as JSON_ERRORS from '../../../errors.json';
import { WsException } from '@nestjs/websockets';

export class MultiLanguageException extends HttpException {
  constructor(customCode: string) {
    const [status, code] = customCode.split('.');

    super(code, +status);
  }
}

export class MultiLanguageWsException extends WsException {
  constructor(customCode: string) {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [_, code] = customCode.split('.');
    const errorToThrow = JSON_ERRORS.filter((error) => {
      return error[code];
    });
    const error = errorToThrow[0][code];
    super(error);
  }
}
