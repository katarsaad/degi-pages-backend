import { ArgumentsHost, Catch, Logger } from '@nestjs/common';
import { BaseWsExceptionFilter } from '@nestjs/websockets';
import { HeaderLanguageEnum } from 'src/common/enums/header-language.enum';
import * as JSON_ERRORS from '../../../errors.json';

@Catch()
export class SocketExceptionFilter extends BaseWsExceptionFilter {
  private readonly logger: Logger;
  constructor() {
    super();
    this.logger = new Logger('SocketException');
  }
  catch(exception: any, host: ArgumentsHost) {
    const ACKCallback = host.getArgByIndex(2);
    if (ACKCallback && typeof ACKCallback === 'function') {
      const errorMessage = this.errorMessage(exception, host); //extracting the error

      this.logger.error(errorMessage?.message || 'Socket error'); //logging the error

      ACKCallback({ success: false, ...errorMessage });
    }
  }

  errorMessage(exception: any, host: ArgumentsHost): any {
    const req = host.switchToWs().getClient();
    const language = req.headers['accept-language'] || HeaderLanguageEnum.EN; //extracting the language
    const response = exception.response;

    this.logger.error(exception);

    if (Array.isArray(exception?.error)) return { message: exception.error[0], code: null }; //if error coming from class validator

    if (exception?.error) return { message: exception.error, code: null };
    if (!response) return { message: 'Socket error', code: null };

    const translatedError = this.WsErrorToThrow(response);

    if (translatedError && language === HeaderLanguageEnum.EN) return { code: translatedError.code, message: translatedError?.multiLanguageError?.EN }; //if error handled

    if (translatedError && language === HeaderLanguageEnum.AR) return { code: translatedError.code, message: translatedError?.multiLanguageError?.EN }; //if error handled
  }

  WsErrorToThrow(code: string) {
    const errorToReturn = JSON_ERRORS.filter((error) => {
      return error[code];
    });
    const error = errorToReturn[0][code];

    return { multiLanguageError: error, code };
  }
}
