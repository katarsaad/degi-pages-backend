import { BadRequestException, Catch, ConflictException, ForbiddenException, Logger, NotFoundException } from '@nestjs/common';
import { BaseExceptionFilter } from '@nestjs/core';
import { Prisma } from '@prisma/client';

@Catch(Prisma.PrismaClientKnownRequestError)
export class PrismaClientExceptionFilter extends BaseExceptionFilter {
  private readonly logger = new Logger('PrismaClientExceptionFilter');

  catch(exception: Prisma.PrismaClientKnownRequestError) {
    const message = exception.message.replace(/\n/g, '');
    const value = JSON.stringify(exception.meta.value);
    const target = JSON.stringify(exception.meta.target);

    this.logger.error(message);

    switch (exception.code) {
      case 'P1010': {
        throw new ForbiddenException("User don't have access on database");
      }
      case 'P2000': {
        throw new BadRequestException(`The provided value for the column is too long for the column's type. Column: ${target} Given value: ${value},`);
      }
      case 'P2002': {
        throw new ConflictException(`The field ${target} should be unique`);
      }
      case 'P2003': {
        const regex = /Foreign key constraint failed on the field: (.+?)/;
        const match = regex.exec(exception.message);
        if (match) throw new BadRequestException(`The field ${match[1]} is invalid`);

        break;
      }
      case 'P2025': {
        throw new NotFoundException(exception.message);
      }

      default:
        throw new BadRequestException('sorry, something went wrong');
    }
  }
}
