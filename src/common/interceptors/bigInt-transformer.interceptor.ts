import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Response<T> {
  data: T;
}

@Injectable()
export class BigIntTransformerInterceptor<T> implements NestInterceptor<T, Response<T>> {
  intercept(_: ExecutionContext, next: CallHandler): Observable<Response<T>> {
    return next.handle().pipe(
      map((data) =>
        JSON.parse(
          JSON.stringify(
            data,
            (_, value) => (typeof value === 'bigint' ? Number(value) : value), // return everything else unchanged
          ),
        ),
      ),
    );
  }
}
