import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { UserContext } from '../dtos/user.context.dto';
export const User = createParamDecorator((data: string, ctx: ExecutionContext): UserContext => {
  const { user } = ctx.switchToHttp().getRequest();
  return data ? user[data] : user;
});
