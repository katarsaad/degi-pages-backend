import { Module } from '@nestjs/common';
import { PrismaService } from './services/prisma.service';
import { EnvironmentService } from './services/environment.service';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';

@Module({ exports: [PrismaService, EnvironmentService, ConfigService, JwtService], providers: [PrismaService, EnvironmentService, ConfigService, JwtService] })
export class CommonModule {}
