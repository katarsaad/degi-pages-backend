import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/common/services/prisma.service';
import { CreateManagerDto } from './dto/create.manager.dto';
import { UpdateManagerDto } from './dto/update.manager.dto';
import { ManagerResponse } from './responses/manager.response';
import { LanguageErrorEnum } from 'src/common/enums/language-error.enum';
import { MultiLanguageException } from 'src/common/exception-filters/multi-language.exception';

@Injectable()
export class ManagerService {
  constructor(readonly prisma: PrismaService) {}

  async createManager(managerData: CreateManagerDto): Promise<ManagerResponse> {
    const existingManager = await this.prisma.manager.findUnique({ where: { email: managerData.email } });
    if (existingManager) {
      throw new MultiLanguageException(LanguageErrorEnum.CREATION_FAILED);
    }
    return this.prisma.manager.create({ data: managerData });
  }

  async findAllManagers(): Promise<ManagerResponse[]> {
    return this.prisma.manager.findMany();
  }

  async findOneManager(id: number): Promise<ManagerResponse | null> {
    const manager = await this.prisma.manager.findUnique({ where: { id } });
    if (!manager) {
      throw new MultiLanguageException(LanguageErrorEnum.MANAGER_NOT_FOUND);
    }
    return manager;
  }

  async updateManager(id: number, managerData: UpdateManagerDto): Promise<ManagerResponse> {
    const manager = await this.prisma.manager.findUnique({ where: { id } });
    if (!manager) {
      throw new MultiLanguageException(LanguageErrorEnum.MANAGER_NOT_FOUND);
    }
    return this.prisma.manager.update({ where: { id }, data: managerData });
  }

  async deleteManager(id: number): Promise<ManagerResponse> {
    const manager = await this.prisma.manager.findUnique({ where: { id } });
    if (!manager) {
      throw new MultiLanguageException(LanguageErrorEnum.MANAGER_NOT_FOUND);
    }
    return this.prisma.manager.delete({ where: { id } });
  }
}
