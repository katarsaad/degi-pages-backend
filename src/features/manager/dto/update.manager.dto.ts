import { ApiProperty } from '@nestjs/swagger';

export class UpdateManagerDto {
  @ApiProperty()
  email?: string;
  @ApiProperty()
  name?: string;
}
