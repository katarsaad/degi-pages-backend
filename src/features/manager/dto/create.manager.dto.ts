import { IsEmail, IsNotEmpty, Length } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateManagerDto {
  @ApiProperty({ example: 'example@email.com' })
  @IsEmail({}, { message: 'Invalid email address.' })
  email: string;

  @ApiProperty({ example: 'John Doe', required: false })
  @IsNotEmpty({ message: 'Name cannot be empty.' })
  name?: string;

  @ApiProperty({ example: 'password123', required: true })
  @IsNotEmpty({ message: 'Password cannot be empty.' })
  @Length(8, 20, { message: 'Password must be between 8 and 20 characters long.' })
  password: string;
}
