import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse, ApiBody } from '@nestjs/swagger';
import { ManagerService } from './manager.service';
import { CreateManagerDto } from './dto/create.manager.dto';
import { UpdateManagerDto } from './dto/update.manager.dto';
import { ManagerResponse } from './responses/manager.response';
import { JwtAuthGuard } from 'src/auth/jwt/jwt.guard';
import { IsPublic } from 'src/auth/decorators/public.decorator';
import { JwtAuth } from 'src/auth/decorators/jwt.auth.decorator';

@ApiTags('managers')
@Controller('managers')
@UseInterceptors(/* Add any interceptors here */)
export class ManagerController {
  constructor(private readonly managerService: ManagerService) {}

  @Post()
  @ApiOperation({ summary: 'Create a new manager' })
  @ApiResponse({ status: 201, description: 'The manager has been successfully created.' })
  @ApiResponse({ status: 400, description: 'Bad Request.' })
  @ApiBody({ type: CreateManagerDto })
  async createManager(@Body() createManagerDto: CreateManagerDto): Promise<ManagerResponse> {
    return this.managerService.createManager(createManagerDto);
  }

  @JwtAuth()
  @Get()
  @ApiOperation({ summary: 'Find all managers' })
  @ApiResponse({ status: 200, description: 'Successfully retrieved all managers.' })
  @ApiResponse({ status: 500, description: 'Internal Server Error.' })
  @UseInterceptors(/* Add any interceptors here */)
  async findAllManagers(): Promise<ManagerResponse[]> {
    return this.managerService.findAllManagers();
  }

  @Get(':id')
  @ApiOperation({ summary: 'Find a manager by ID' })
  @ApiResponse({ status: 200, description: 'Successfully retrieved the manager.' })
  @ApiResponse({ status: 404, description: 'Manager not found.' })
  @UseInterceptors(/* Add any interceptors here */)
  async findOneManager(@Param('id', ParseIntPipe) id: number): Promise<ManagerResponse | null> {
    return this.managerService.findOneManager(id);
  }
  @IsPublic()
  @UseGuards(JwtAuthGuard)
  @Put(':id')
  @ApiOperation({ summary: 'Update a manager' })
  @ApiResponse({ status: 200, description: 'Successfully updated the manager.' })
  @ApiResponse({ status: 400, description: 'Bad Request.', type: ManagerResponse })
  @ApiBody({ type: UpdateManagerDto })
  @UseInterceptors(/* Add any interceptors here */)
  async updateManager(@Param('id', ParseIntPipe) id: number, @Body() updateManagerDto: UpdateManagerDto): Promise<ManagerResponse> {
    return this.managerService.updateManager(id, updateManagerDto);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete a manager' })
  @ApiResponse({ status: 200, description: 'Successfully deleted the manager.' })
  @ApiResponse({ status: 404, description: 'Manager not found.' })
  @UseInterceptors(/* Add any interceptors here */)
  async deleteManager(@Param('id', ParseIntPipe) id: number): Promise<ManagerResponse> {
    return this.managerService.deleteManager(id);
  }
}
