import { ApiProperty } from '@nestjs/swagger';

export class ManagerResponse {
  @ApiProperty()
  email: string;
  @ApiProperty()
  name: string;
}
