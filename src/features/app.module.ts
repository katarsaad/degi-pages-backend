import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { CommonModule } from '../common/common.module';
import { ManagerModule } from './manager/manager.module';
import { AuthModule } from '../auth/auth.module';
// import { APP_GUARD } from '@nestjs/core/constants';
// import { JwtAuthGuard } from './auth/jwt/jwt.guard';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { ExtrasModule } from '../extras/extras.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true, // Make ConfigModule available globally
      envFilePath: '.env', // Path to your .env file
    }),
    CommonModule,
    ManagerModule,
    AuthModule,
    ExtrasModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
