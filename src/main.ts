import { NestFactory } from '@nestjs/core';
import { AppModule } from './features/app.module';
import { initSwaggerConfig } from './config/swagger.cofig';
import { initGlobalConfig } from './config/global.config';
import * as dotenv from 'dotenv';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  dotenv.config();

  initSwaggerConfig(app);
  initGlobalConfig(app);

  await app.listen(3000);
}
bootstrap();
