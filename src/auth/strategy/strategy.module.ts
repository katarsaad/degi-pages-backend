import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './strategy.service';

@Module({
  imports: [PassportModule],
  providers: [JwtStrategy],
})
export class StrategyModule {}
