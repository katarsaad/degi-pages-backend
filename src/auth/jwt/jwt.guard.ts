import { Injectable, ExecutionContext, UnauthorizedException, ForbiddenException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { JwtService } from '@nestjs/jwt';
import { PrismaService } from 'src/common/services/prisma.service';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(
    private reflector: Reflector,
    private jwtService: JwtService,
    private prismaService: PrismaService,
  ) {
    super();
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const isPublic = this.reflector.getAllAndOverride<boolean>('isPublic', [context.getHandler(), context.getClass()]);
    if (isPublic) {
      return true;
    }

    const request = this.getRequest(context);
    if (!request.headers) {
      throw new UnauthorizedException('Headers not found');
    }

    const token = this.extractTokenFromHeader(request);
    if (!token) {
      throw new UnauthorizedException('Token not found');
    }

    const isTokenValid = this.validateToken(token);
    if (!isTokenValid) {
      throw new UnauthorizedException('Token is not valid');
    }

    const decodedToken = this.jwtService.decode(token) as any;
    if (!decodedToken) {
      throw new UnauthorizedException('Invalid token');
    }

    const manager = await this.prismaService.manager.findUnique({
      where: { id: decodedToken.sub },
      select: { id: true, email: true, name: true },
    });
    if (!manager) {
      throw new ForbiddenException('Manager not found');
    }

    request.user = { name: manager.name, email: manager.email, id: manager.id };

    // Call handleRequest explicitly to modify the request object
    return this.handleRequest(null, manager, null, context);
  }

  public getRequest(context: ExecutionContext) {
    const httpContext = context.switchToHttp();
    const request = httpContext.getRequest();
    return request;
  }

  private extractTokenFromHeader(request): string | null {
    const authHeader = request.headers['authorization'];
    if (!authHeader) {
      return null;
    }
    const parts = authHeader.split(' ');
    if (parts.length !== 2 || parts[0] !== 'Bearer') {
      return null;
    }
    return parts[1];
  }

  private validateToken(token: string): boolean {
    try {
      const decoded = this.jwtService.verify(token, { secret: process.env.JWT_SECRET_KEY });
      return !!decoded;
    } catch (error) {
      console.error('Token validation error:', error);
      return false;
    }
  }
}
