import { UseGuards, applyDecorators } from '@nestjs/common';
import { ApiBearerAuth, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { JwtAuthGuard } from '../jwt/jwt.guard';

export function JwtAuth() {
  return applyDecorators(ApiBearerAuth(), ApiUnauthorizedResponse(), UseGuards(JwtAuthGuard));
}
