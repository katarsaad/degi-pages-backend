import { Controller, Post, Body } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse, ApiBody } from '@nestjs/swagger';
import { RegisterDto } from './dto/register.dto';
import { ManagerAuthResponse } from './responses/manager.auth.response';
import { AuthService } from './auth.service';
import { TokenResponse } from './responses/token.response';
import { LoginDto } from './dto/login.dto';
import { IsPublic } from './decorators/public.decorator';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}
  @IsPublic()
  @Post('register')
  @ApiOperation({ summary: 'Register a new user' })
  @ApiResponse({ status: 201, description: 'The user has been successfully registered.' })
  @ApiResponse({ status: 400, description: 'Bad Request.' })
  @ApiBody({ description: 'User registration data', type: RegisterDto })
  async register(@Body() registerDto: RegisterDto): Promise<ManagerAuthResponse> {
    return await this.authService.register(registerDto);
    // Registration logic here
  }

  @IsPublic()
  @Post('login')
  @ApiOperation({ summary: 'Log in a user' })
  @ApiResponse({ status: 200, description: 'Successful login.' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiBody({ type: LoginDto })
  async login(@Body() dto: LoginDto): Promise<TokenResponse> {
    return await this.authService.login(dto.email, dto.password);
    // Login logic here
  }
}
