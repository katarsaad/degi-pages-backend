import { ApiProperty } from '@nestjs/swagger';
import { ManagerResponse } from 'src/features/manager/responses/manager.response';

export class ManagerAuthResponse {
  manager: ManagerResponse;
  @ApiProperty()
  bearerToken: string;
}
