import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/common/services/prisma.service';
import { JwtService } from '@nestjs/jwt';
import { EnvironmentService } from 'src/common/services/environment.service';
import { TokenResponse } from './responses/token.response';
import { ManagerAuthResponse } from './responses/manager.auth.response';
import { RegisterDto } from './dto/register.dto';
import { MultiLanguageException } from 'src/common/exception-filters/multi-language.exception';
import { LanguageErrorEnum } from 'src/common/enums/language-error.enum';

@Injectable()
export class AuthService {
  constructor(
    private prisma: PrismaService,
    private jwtService: JwtService,
    private environmentService: EnvironmentService,
  ) {}

  async register(registerDto: RegisterDto): Promise<ManagerAuthResponse> {
    const { password, email, name } = registerDto;
    const manager = await this.prisma.manager.findFirst({ where: { email } });
    if (manager) {
      throw new MultiLanguageException(LanguageErrorEnum.MANAGER_EXIST);
    }
    const hashedPassword = await this.hashPassword(password);

    try {
      const manager = await this.prisma.manager.create({
        data: {
          email,
          name: name,
          password: hashedPassword,
        },
      });

      const bearerToken = await this.generateBearerToken(Number(manager.id));
      return { manager, bearerToken };
    } catch (error) {
      throw new MultiLanguageException(LanguageErrorEnum.CREATION_FAILED);
    }
  }

  async login(email: string, password: string): Promise<TokenResponse> {
    const manager = await this.prisma.manager.findUnique({
      where: {
        email,
      },
    });

    if (!manager) {
      throw new MultiLanguageException(LanguageErrorEnum.MANAGER_NOT_FOUND);
    }

    const isPasswordValid = await this.comparePassword(password, manager.password);

    if (!isPasswordValid) {
      throw new MultiLanguageException(LanguageErrorEnum.INVALID_PASSWORD);
    }

    const bearerToken = await this.generateBearerToken(manager.id);
    return { bearerToken };
  }

  private async hashPassword(password: string): Promise<string> {
    return password;
  }

  private async comparePassword(plainPassword: string, hashedPassword: string): Promise<boolean> {
    return plainPassword === hashedPassword;
  }

  async generateBearerToken(userId: number, expiresIn?: string | number): Promise<string> {
    const payload = { sub: userId };

    return await this.jwtService.signAsync(payload, {
      secret: this.environmentService.jasonWebTokenConfig.JWTSecretKey,
      expiresIn: expiresIn ?? '10 days',
    });
  }
}
