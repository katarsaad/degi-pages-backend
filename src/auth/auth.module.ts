import { Module } from '@nestjs/common';
import { StrategyModule } from './strategy/strategy.module';
import { JwtAuthModule } from './jwt/jwt.module';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { CommonModule } from 'src/common/common.module';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategy/strategy.service';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),

    JwtModule.register({
      secret: process.env.JWT_SECRET_KEY,
      signOptions: { expiresIn: '60h' },
    }),
    StrategyModule,
    JwtAuthModule,
    CommonModule,
    JwtModule,
    PassportModule,
  ],
  providers: [AuthService, JwtStrategy],
  exports: [AuthService, JwtModule], // Export JwtModule to make JwtService available
  controllers: [AuthController],
})
export class AuthModule {}
