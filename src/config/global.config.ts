import { ValidationPipe } from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';
import { HttpExceptionFilter } from 'src/common/exception-filters/http-exception.filter';
import { PrismaClientExceptionFilter } from 'src/common/exception-filters/prisma-client-exception.filter';
import { BigIntTransformerInterceptor } from 'src/common/interceptors/bigInt-transformer.interceptor';

export function initGlobalConfig(app) {
  const { httpAdapter } = app.get(HttpAdapterHost);
  app.useGlobalFilters(new HttpExceptionFilter(), new PrismaClientExceptionFilter(httpAdapter));
  app.useGlobalInterceptors(new BigIntTransformerInterceptor());

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
      transform: true,
    }),
  );
}
