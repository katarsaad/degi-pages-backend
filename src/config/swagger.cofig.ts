import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
export function initSwaggerConfig(app) {
  const config = new DocumentBuilder()
    .setTitle('Smoking Cessation API')
    .setDescription('Smoking Cessation Backend API description')
    .setVersion('1.0')
    .addBearerAuth()
    .addApiKey(
      {
        type: 'apiKey',
        name: 'Accept-Language',
        in: 'header',
      },
      'Accept-Language',
    )
    .build();
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api-docs', app, document); // This will make Swagger docs available at /api-docs
}
