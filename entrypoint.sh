#!/bin/sh

# Wait until MySQL is ready
until nc -z -v -w30 db 3306
do
  echo "Waiting for database connection..."
  sleep 1
done

# Run Prisma migrations
npx prisma generate
npx prisma db push --accept-data-loss
npx prisma db seed

# Start the application
npm run start
